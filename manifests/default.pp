include git

node default {
    class { 'nginx':}
    nginx::resource::vhost { 'localhost':
        ensure => present,
        listen_port => 8001,
        server_name => ['localhost.localdomain localhost'],
        www_root => '/var/www/puppetlabs',
        notify => Service['nginx'],
    }
    
    vcsrepo {
        '/var/www/puppetlabs':
            ensure => present,
            provider => git,
            source => 'https://github.com/puppetlabs/exercise-webpage',
    }
}